/**
 * Created by anzietek on 2015-12-08.
 */
'use strict';
angular.module('myApp.GroupsController', ['ngRoute', 'ngResource', 'myApp.config']).
factory('groupsUrl', function (PROTOCOL, HOST, PORT) {
    return PROTOCOL + '://' + HOST + ':' + PORT + '/orangepi/api/1.0/scheduler/show/groups';
}).
factory('showSchedulerUrl', function (PROTOCOL, HOST, PORT) {
    return PROTOCOL + '://' + HOST + ':' + PORT + '/orangepi/api/1.0/scheduler/show/:id';
}).
factory('modifySchedulerUrl', function (PROTOCOL, HOST, PORT) {
    return PROTOCOL + '://' + HOST + ':' + PORT + '/orangepi/api/1.0/scheduler/modify';
}).
factory('mongoGSaveUrl', function (PROTOCOL, HOST, PORT) {
    return PROTOCOL + '://' + HOST + ':' + PORT + '/orangepi/api/1.0/mongodb/save';
}).
factory('mongoGLoadUrl', function (PROTOCOL, HOST, PORT) {
    return PROTOCOL + '://' + HOST + ':' + PORT + '/orangepi/api/1.0/mongodb/load/:id';
}).
config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/groups/:id', {
        templateUrl: 'views/groups.html',
        controller: 'GroupsController'

    }).when('/groups/delay/:delay_id', {
        templateUrl: 'views/groupdelay.html',
        controller: 'GroupDelayController'
    }).when('/groups/mode/:mode_id', {
        templateUrl: 'views/groupmode.html',
        controller: 'GroupModeController'
    }).when('/groups/weather/:weather_id', {
        templateUrl: 'views/groupweather.html',
        controller: 'GroupWeatherController'
    });

}]).factory('ModifyScheduler', function ($resource, modifySchedulerUrl) {
    var data = $resource(modifySchedulerUrl, {}, {
            save: {
                method: 'POST',
                data: '@data'

            },
        },
        {stripTrailingSlashes: true});
    return data;
}).factory('MongoGSave', function ($resource, mongoGSaveUrl) {
        var data = $resource(mongoGSaveUrl, {}, {
                save: {
                    method: 'POST',
                    data: '@data'

                },

            },
            {stripTrailingSlashes: true});
        return data;
    }
).factory('MongoGLoad', function ($resource, mongoGLoadUrl) {
    var data = $resource(mongoGLoadUrl, {id: '@id'});
    return data;
}).
factory('ShowScheduler', function ($resource, showSchedulerUrl) {
    var data = $resource(showSchedulerUrl, {
        id: '@id'
    });
    return data;
}).
factory('OnChange', function ($rootScope) {
    var onChange = {};
    onChange.emit = function (data, id) {
        if (data !== undefined) {
            $rootScope.$broadcast('onChange', {id: id});
        }
    };
    return onChange;
}).
service('SharedService', function ($rootScope) {
    var shared = {enables: [], dbOp: {enable: false}};
    shared.setGroups = function (data) {
        this.groups = data;
    };
    shared.getGroups = function () {
        return this.groups;
    };
    shared.deleteGroups = function () {
        delete this.groups;
    };

    $rootScope.$on('onChange', function (event, args) {
        shared.enables[args.id] = true;
        shared.dbOp.enable = false;

    });

    return shared;
}).controller('GroupsController', function ($scope, $routeParams, $location, $timeout, $q, SharedService,
                                            ModifyScheduler, ShowScheduler,
                                            MongoGSave, MongoGLoad) {


        $scope.group_type = [
            {value: 'inclusive', text: 'inclusive'},
            //{value: 'exclusive', text: 'exclusive'}
        ];
        $scope.boolean = [
            {value: true, text: true},
            {value: false, text: false}
        ];

        //$scope.groupsResource = $resource(groupsUrl);
        $scope.listGroups = function () {
            $scope.groups = ShowScheduler.query({id: 'groups'}); //$scope.groupsResource.query();
            SharedService.setGroups($scope.groups);

        };


        if (angular.isDefined(SharedService.groups)) {
            $scope.groups = SharedService.groups;
        } else {
            $scope.listGroups();
        }
        $scope.ids = [];
        $scope.lineIds = function () {
            if ($scope.ids.length !== 0) {
                return $scope.ids;
            }
            angular.forEach($scope.groups, function (value, key) {
                $scope.ids.push({value: value.id, text: value.id});
            });
            $scope.ids.sort(function (a, b) {
                return a.value - b.value;
            });
            return $scope.ids;
        };

        $scope.enables = SharedService.enables;


        $scope.sendToServer = function (group) {
            if (group.mode.__class__ === 'ManualMode') {
                delete group.mode.trigger;
            }
            //console.log(angular.toJson(group));
            ModifyScheduler.save({group: group}).$promise.then(
                function (data) {

                    ShowScheduler.query({id: 'groups'}).$promise.then(function (data) {
                        $scope.groups = data;
                        $scope.enables[group.id] = SharedService.enables[group.id] = false;
                    });
                }
            );
};
        $scope.radioBoxSelection = {id: $routeParams.id};

        $scope.isRadioBoxSelect = function (index) {
            return $scope.radioBoxSelection.id == index;
        };

        $scope.dbOp = SharedService.dbOp;
        $scope.dbSend = function () {
            ModifyScheduler.save({groups: angular.toJson($scope.groups)});
            $scope.dbOp.enable = false;
        }
        $scope.dbSave = function () {
            MongoGSave.save({db_groups: angular.toJson(SharedService.groups)});
        };
        $scope.dbLoad = function () {
            MongoGLoad.get({id: 'groups'}).$promise.then(function (data) {
                if (data.exception === null) {
                    SharedService.setGroups(data.groups);
                    $scope.groups = data.groups;
                    $scope.dbOp.enable = true;
                    angular.forEach(SharedService.enables, function (data, key) {
                        SharedService.enables[key] = false;
                    });
                }
            });
        };
    }
).controller('GroupDelayController', function ($scope, $routeParams, $location, SharedService, SEC_SHOW) {
    //$scope.$on('emitGroups', function () {
    //    $scope.groups = SharedService.groups;
    //});
    //$scope.updateDelay = function (data) {
    //    if (typeof data == 'undefined') {
    //        //  console.log(data);
    //        ;
    //    }
    //}
    if (!angular.isDefined(SharedService.groups)) {
        $location.path('/groups');
        return;
    }
    $scope.groups = SharedService.groups;
    $scope.sec_show = SEC_SHOW;
    var delay_id = $routeParams.delay_id;
    if (angular.isDefined(delay_id)) {
        angular.forEach($scope.groups, function (value, key) {
            if (delay_id == value.delay.delay_id) {
                $scope.group = value;
                return;
            }
        });

    }

}).controller('GroupModeController', function ($scope, $routeParams, $location, $filter, SharedService, SEC_SHOW) {
    if (!angular.isDefined(SharedService.groups)) {
        $location.path('/groups');
        return;
    }
    $scope.sec_show = SEC_SHOW;
    $scope.mode_mode = [
        {value: 'auto', text: 'auto'},
        {value: 'manual', text: 'manual'}
    ];
    $scope.trigger_type = [
        {value: 'Interval', text: 'Interval'},
        {value: 'Cron', text: 'Cron'}
    ];
    var action_auto = [
        {value: 'on_off', text: 'on_off'},
        //{value: 'off_on', text: 'off_on'}
    ];
    var action_manual = [
        {value: 'on', text: 'on'},
        {value: 'off', text: 'off'}
    ];
    $scope.timezones = [
        {value: 'Europe/Warsaw', text: 'Europe/Warsaw'},
        //{value: 'UTC', text: 'UTC'}
    ];

    $scope.repeatModel={
        enable:false
    };

    $scope.day_of_weeks = [
        {value: 0, text: 'Pon.'},
        {value: 1, text: 'Wt.'},
        {value: 2, text: 'Śr.'},
        {value: 3, text: 'Czw.'},
        {value: 4, text: 'Pt.'},
        {value: 5, text: 'Sob.'},
        {value: 6, text: 'Nd.'}];

    $scope.displayDays = function (obj) {
        var days = ['Pon.', 'Wt.', 'Śr.', 'Czw.', 'Pt.', 'Sob.', 'Nd.'];
        var result = '*';//'Pon.- Nd.';
        if (angular.isArray(obj)) {
            obj.sort(function (a, b) {
                return a - b;
            });
            var tmp = []
            angular.forEach(obj, function (data) {
                tmp.push(days[data]);
            });
            if(tmp.length === days.length){
                result = 'Pon.- Nd.';
            } else if (tmp.length) {
                result = tmp.join();
            }
        }
        return result;
    };
    var editObj;
    $scope.edit = function (form) {
        editObj = $scope.group;
        form.$show();
    }

    $scope.cancel = function (form) {
        $scope.group = editObj;
        form.$cancel();
    };

    //action_manual = action_manual.concat(action_auto);
    $scope.checkValue = function (data) {
        if (data === null || data === undefined) {
            return 'Nie wybrano akcji';
        }
    };
    $scope.updateModeAction = function (data) {

        if (data == 'auto') {


            $scope.mode_action = action_auto;
            $scope.group.mode.__class__ = 'AutoMode';
            if (!angular.isDefined($scope.group.mode.trigger)) {
                var trigger = {__bases__: []};
                $scope.group.mode.trigger = trigger;
                $scope.group.mode.trigger.__bases__[0] = {
                    __module__: 'org.device.json.object.trigger',
                    __class__: 'Trigger',
                    timezone: 'Europe/Warsaw',
                    trigger_id: 'user-defined'
                };
            }
            $scope.isAuto = true;
        } else if (data == 'manual') {
            $scope.mode_action = action_manual;//action_manual;
            $scope.group.mode.__class__ = 'ManualMode';
            $scope.isAuto = false;

        }
    };

    $scope.updateView = function (data) {
        $scope.is_interval = data == 'Interval' ? true : false;
    };

    $scope.cancelForm = function (form) {
        $scope.group.mode = $scope.prevMode;
        $scope.is_interval = $scope.sharedInterval
        $scope.isAuto =$scope.sharedAuto;
        //console.log(angular.toJson($scope.group));
        //$scope.updateModeAction($scope.group.mode.__bases__[0].mode)



        form.$cancel();

    };
    $scope.editForm = function (form) {
        $scope.editObj = $scope.group.mode;
        $scope.sharedInterval = $scope.is_interval;
        $scope.sharedAuto = $scope.isAuto
        form.$show();
    }


    //
    //$scope.$watch("group.mode.__bases__[0].mode", function(data){
    //
    //    //$scope.updateModeAction(data);
    //});
    var mode_id = $routeParams.mode_id;
    $scope.groups = SharedService.groups;
    if (angular.isDefined(mode_id)) {
        angular.forEach($scope.groups, function (value, key) {
            if (mode_id == value.mode.__bases__[0].mode_id) {
                $scope.group = value;
                $scope.prevMode = $scope.group.mode;
                $scope.is_interval = true;
                //console.log(angular.toJson($scope.group));
                //if ($scope.group.mode.trigger === undefined) {
                //    $scope.group.mode.trigger = {
                //        "__bases__": [
                //            {
                //                "__class__": "Trigger",
                //                "__module__": "org.device.json.object.trigger",
                //                "end_date": null,
                //                "start_date": null,
                //                "timezone": "utc"
                //            }
                //        ],
                //        "__class__": "Interval",
                //        "__module__": "org.device.json.object.trigger",
                //        "days": 0,
                //        "hours": 0,
                //        "minutes": 0,
                //        "seconds": 0,
                //        "weeks": 0
                //    };
                //
                //}
                //console.log($scope.group.mode.trigger.__class__ );
                $scope.updateModeAction($scope.group.mode.__bases__[0].mode);
                if ($scope.group.mode.trigger !== undefined) {
                    if ($scope.group.mode.trigger.__class__ === 'Interval') {
                        $scope.is_interval = true;
                    } else {
                        $scope.is_interval = false;
                    }
                }

            }
        });
    }

}).controller('GroupWeatherController', function ($scope, $routeParams, $location, SharedService) {
    if (!angular.isDefined(SharedService.groups)) {
        $location.path('/groups');
        return;
    }
    $scope.boolean = [
        {value: true, text: true},
        {value: false, text: false}
    ];
    var weather_id = $routeParams.weather_id;
    $scope.groups = SharedService.groups;
    if (angular.isDefined($routeParams.weather_id)) {
        var group = [];
        angular.forEach($scope.groups, function (value, key) {
            if (weather_id == value.weather_td.id) {
                (function (id, name) {
                    var obj = {};
                    obj.id = id;
                    obj.name = name;
                    $scope.group = obj;
                })(value.id, value.name);
                $scope.weather = value;
                $scope.weather_td = value.weather_td;
            }
        });

    }
    $scope.updateView = function (data) {
        if (data === true) {
            $scope.weather.weather_on = true;
        } else {
            $scope.weather.weather_on = false;
        }

    };
});