/**
 * Created by AndrzejZ on 08.06.2017.
 */
/**
 * Created by AndrzejZ on 13.04.2017.
 */
'use strict';
angular.module('myApp.DeviceController', ['ngRoute', 'ngResource', 'myApp.config']).
config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/home', {
        templateUrl: 'views/devicestatus.html',
        controller: 'DeviceController'
    });
}]).
factory('StatusScheduler', function ($resource, showSchedulerUrl) {
    var data = $resource(showSchedulerUrl, {
            id: '@id'
        },
        {query: {method: "GET", isArray: false}});
    return data;
}).factory('StatusStream', function ($websocket, WS_PROT, HOST, WS_PORT) {

        var webStream = $websocket(WS_PROT + '://' + HOST + ':' + WS_PORT + '/status');
        var data = [];
        webStream.onMessage(function (event) {
            var obj = angular.fromJson(event.data)
            data.splice(0, data.length);
            angular.forEach(obj.stats, function (value, key) {
                data.push(value);
            });
            data.sort(function (a, b) {
                return a.id - b.id;
            });

        });

        return {
            data: data,
            status: function () {
                return webStream.readyState;

            },
            clean: function () {
                data = [];
            }

        };
    })
    .controller('DeviceController', function ($scope, StatusStream, StatusScheduler) {

        $scope.data = StatusStream.data;
        $scope.dev = {};//{val:ShowScheduler.query({id: 'running'})}
        $scope.inArr=[4,5];
        // $scope.devStatus= function(){
        //  $scope.dev={stat:ShowScheduler.query({id: 'running'})};
        // }
        StatusScheduler.query({id: 'status'}).$promise.then(function (data) {
            $scope.dev = data; //angular.fromJson(data);
            //console.log(data);
        });


    });