/**
 * Created by AndrzejZ on 23.02.2016.
 */
'use strict';
angular.module('myApp.WeatherJobController', ['ngRoute', 'ngResource', 'myApp.config']).
factory('modifyWeatherJobUrl', function (PROTOCOL,HOST, PORT) {
    return PROTOCOL + '://' + HOST + ':' + PORT + '/orangepi/api/1.0/scheduler/modify';
}).
factory('weatherJobUrl', function (PROTOCOL,HOST, PORT) {
    return PROTOCOL + '://' + HOST + ':' + PORT + '/orangepi/api/1.0/scheduler/show/:id';
}).factory('ShowWeatherJob', function ($resource, weatherJobUrl) {
    var data = $resource(weatherJobUrl, {id: '@id'});
    return data;
}).config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/weatherjob', {
        templateUrl: 'views/weatherjob.html',
        controller: 'WeatherJobController'

    });

}]).factory('ModifyWeatherJob', function ($resource, modifyWeatherJobUrl) {
    var data = $resource(modifyWeatherJobUrl, {}, {
            save: {
                method: 'POST',
                data: '@data'

            },
        },
        {stripTrailingSlashes: true});
    return data;
}).
controller('WeatherJobController', function ($scope, ShowWeatherJob,ModifyWeatherJob) {
    $scope.weatherJob = ShowWeatherJob.get({id: 'weather'});
    //console.log($scope.weatherJob);
    $scope.enabled = false;
    $scope.boolean = [
        {value: true, text: true},
        {value: false, text: false}
    ];
    $scope.country = [
        {value: 'pl', text: 'pl'}
    ];
    $scope.enableSendButton = function () {
        $scope.enabled = true;

    }
    $scope.sendToServer = function (data) {
     //  console.log({weather: angular.toJson(data)});
        ModifyWeatherJob.save({weather: data});
        $scope.enabled = false;
    }
    //console.log($scope.weatherJob);
});