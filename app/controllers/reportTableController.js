/**
 * Created by anzietek on 2016-01-14.
 */
'use strict';

angular.module('myApp.ReportTableController', ['ngRoute', 'ngResource', 'ngWebSocket', 'myApp.config']).
config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/reporttable', {
        controller: 'ReportTableController',
        templateUrl: 'views/reporttable.html'
    });


}]).
//'ws://localhost:8888/ws'
factory('Stream', function ($websocket, WS_PROT, HOST, WS_PORT, MongoGLoad) {

    var webStream = $websocket(WS_PROT + '://' + HOST + ':' + WS_PORT + '/report');
    var data = [];
    MongoGLoad.get({id: 'reports'}).$promise.then(function (d) {
        if (d.exception === null) {
            angular.forEach(d.reports, function (value, key) {
                data.push(value);

            });
        }
    });
    webStream.onMessage(function (event) {
        data.unshift(angular.fromJson(event.data));
    });

    return {
        data: data,
        status: function () {
            return webStream.readyState;

        },
        clean: function () {
            data = [];
        }
    };
}).
controller('ReportTableController', function ($scope, Stream, MongoGLoad) {
    $scope.pagination = {page: 0};
    $scope.data = Stream.data;

    $scope.showTable = function (index) {
        $scope.pagination.page = index;
    };
});
