/**
 * Created by AndrzejZ on 11.04.2017.
 */
'use strict';
angular.module('myApp.PompController', ['ngRoute', 'ngResource', 'myApp.config']).
factory('showPompUrl', function (PROTOCOL, HOST, PORT) {
    return PROTOCOL + '://' + HOST + ':' + PORT + '/orangepi/api/1.0/pomp/show';
}).
factory('modifyPompUrl',function (PROTOCOL, HOST, PORT) {
    return PROTOCOL + '://' + HOST + ':' + PORT + '/orangepi/api/1.0/pomp/modify';
}).factory('ModifyPomp', function ($resource, modifyPompUrl) {
    var data = $resource(modifyPompUrl, {}, {
            save: {
                method: 'POST',
                data: '@data'

            },

        },
        {stripTrailingSlashes: true});
    return data;
}).
config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/pomp', {
        templateUrl: 'views/pomp.html',
        controller: 'PompController'
    });
}]).
controller('PompController',function ($scope, $resource, showPompUrl,ModifyPomp){

    $scope.selectionTypes = [{id: 0, value: 'Pompa'},
        //      {id: 1, value: 'Konsola'}
    ];
    $scope.type = {};
    $scope.OnOff = [
        {id: false, value: 'Off'},
        {id: true, value: 'On'}
    ];
    $scope.pomp = $resource(showPompUrl).get();
    $scope.enables = [];
    $scope.enableSend = function (id, data) {
        $scope.enables[id] = true;
    };
    $scope.buttons = [true];
    $scope.beforeSave = function (id, form) {
        $scope.buttons[id] = true;
    };
    $scope.edit = function (id, form) {
        $scope.buttons[id] = false;
        form.$show();
    };
    $scope.cancel = function (id, form) {
        $scope.buttons[id] = true;
        form.$cancel();
    };
    $scope.sendToServer = function (id, data) {
        //console.log(angular.toJson(data));
        $scope.enables[id] = false;
        $scope.pomp=ModifyPomp.save(angular.toJson(data));
       // console.log(angular.toJson($scope.pomp));
    }
});