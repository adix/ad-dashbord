/**
 * Created by anzietek on 2015-12-09.
 */
'use strict';
angular.module('myApp.filter', []).
filter("strLength", function () {
    return function (data, length) {
        if (angular.isString(data)) {
            return data.substr(0, length);

        }
        return data;
    };

}).
filter('toArray', function () {
    return function (obj, addKey) {
        if (!(obj instanceof Object)) {
            return obj;
        }

        if (addKey === false) {
            return Object.values(obj);
        } else {
            return Object.keys(obj).map(function (key) {
                if (!(obj[key] instanceof Object)) {
                    return;
                }
                return Object.defineProperty(obj[key], '$key', {enumerable: false, value: key});
            });
        }
    };
}).filter('removeObj', function () {
        return function(inputArray, IDs) {
            return inputArray.filter(function (entry) {
                return entry.id !== IDs;
            }, IDs); // filterIDs here is what "this" is referencing in the line above
        };
});