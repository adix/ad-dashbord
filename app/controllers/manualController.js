/**
 * Created by AndrzejZ on 09.06.2017.
 */
'use strict';
angular.module('myApp.ManualController', ['ngRoute', 'ngResource', 'myApp.config']).
config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/manual', {
        templateUrl: 'views/manualcontrol.html',
        controller: 'ManualController'
    });
}]).factory('manualUrl', function (PROTOCOL, HOST, PORT) {
    return PROTOCOL + '://' + HOST + ':' + PORT + '/orangepi/api/1.0/manual/modify';
}).factory('Manual', function ($resource, manualUrl) {
        var data = $resource(manualUrl, {}, {
                save: {
                    method: 'POST',
                    data: '@data'

                },
            },
            {stripTrailingSlashes: true});
        return data;
    })
    .controller('ManualController', function ($scope, StatusStream,Manual) {

        $scope.data = StatusStream.data;
        //$scope.data.filter(function(obj){
        //    console.log(obj.id);
        //    if(obj.id!==5) {
        //        console.log(obj.id);
        //        return obj;
        //    }
        //});
        $scope.inArr=[4,5];
        $scope.switch = function (id, val) {
            var to_send = {
                relay: {
                    id: id, on_off: val,
                    __class__:'RelayDTO'
                }
            };
            Manual.save(to_send);
            //console.log(angular.toJson(to_send));
        };
    });