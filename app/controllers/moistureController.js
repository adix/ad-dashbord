/**
 * Created by AndrzejZ on 13.04.2017.
 */
'use strict';
angular.module('myApp.MoistureController', ['ngRoute', 'ngResource', 'myApp.config']).
    config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/moisture', {
            templateUrl: 'views/moisture.html',
            controller: 'MoistureController'
        });
    }])
    .controller('MoistureController', function ($scope, ShowScheduler, ModifyScheduler) {
        $scope.msensor = ShowScheduler.get({id: 'moisture'});
        //console.log(angular.toJson($scope.msensor));
        $scope.selectionTypes = [{id: 0, value: 'Czujnik wilgotnosci'},
            //      {id: 1, value: 'Konsola'}
        ];
        $scope.type = {};
        $scope.OnOff = [
            {id: false, value: 'Off'},
            {id: true, value: 'On'}
        ];
        $scope.state = [
            {id: 0, value: 'NO'},
            {id: 1, value: 'NC'}
        ];
        $scope.enables = [];
        $scope.enableSend = function (id, data) {
            $scope.enables[id] = true;
        };
        $scope.buttons = [true];
        $scope.beforeSave = function (id, form) {
            $scope.buttons[id] = true;
        };
        $scope.edit = function (id, form) {
            $scope.buttons[id] = false;
            form.$show();
        };
        $scope.cancel = function (id, form) {
            $scope.buttons[id] = true;
            form.$cancel();
        };
        $scope.sendToServer = function (id, data) {
            //console.log(angular.toJson(data));
            $scope.enables[id] = false;
            $scope.pomp = ModifyScheduler.save({moisture: data});
            //console.log({moisture: data});
        }
    });