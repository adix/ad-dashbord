/**
 * Created by anzietek on 2015-12-24.
 */

'use strict';
angular.module('myApp.StatusTaskController', ['ngRoute', 'ngResource', 'myApp.config']).
factory('taskStatusUrl', function (PROTOCOL,HOST, PORT) {
    return PROTOCOL + '://' + HOST + ':' + PORT + '/orangepi/api/1.0/tasks';
}).
config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/statustasks', {
        templateUrl: 'views/statustask.html',
        controller: 'StatusTaskController'
    });

}]).
factory('Tasks', function ($resource, taskStatusUrl) {
    var data = $resource(taskStatusUrl);
    return data;
}).
controller('StatusTaskController', function ($scope, Tasks) {

    var promise = Tasks.get().$promise;
    $scope.pagination = {page: 0};
    $scope.jobs = [];
    promise.then(function (response) {
        var jobs = [];
        angular.forEach(response.jobs, function (value, key) {
            var props = [];
            var val;
            var properties = Object.keys(value);
            var timestamp = 0;
            for (var i = 0, n = properties.length; i < n; i++) {
                if (properties[i] === 'timestamp') {
                    timestamp = value[properties[i]];
                    val = new Date(value[properties[i]] * 1000);
                } else {
                    val = value[properties[i]];
                }
                props.push({name: properties[i], value: val});
            }
            jobs.push({
                timestamp: timestamp,
                props: props,
            });

        });
        jobs.sort(function (a, b) {
            return b.timestamp - a.timestamp;
        });
        $scope.jobs = jobs;
    });
    $scope.showTable = function (index) {
        $scope.pagination.page = index;
    };

});