'use strict';

// Declare app level module which depends on views, and components

angular.module('myApp', [
    'ngRoute',
    'xeditable',
    'checklist-model',
    'ui.bootstrap',
    'myApp.GroupsController',
    'myApp.OperationController',
    'myApp.StatusTaskController',
    'myApp.ReportConfController',
    'myApp.ReportTableController',
    'myApp.WeatherJobController',
    'myApp.PompController',
    'myApp.MoistureController',
    'myApp.DeviceController',
    'myApp.ManualController',
    //'myApp.ChartController',
    //'myApp.LinearChart',
    'myApp.filter',
    'myApp.version'
]).
config(['$routeProvider', function ($routeProvider) {
    $routeProvider.otherwise({redirectTo: '/home'});
}]).run(function (editableOptions) {
    editableOptions.theme = 'bs2';
}).controller('RootController', function ($scope, $location, OnChange) {
    $scope.onChange = function (data, id) {
        OnChange.emit(data, id);
    };
    $scope.back = function (id) {

        $location.path('/groups/' + id);
    };
    $scope.checkRange = function (data, min, max, canUndefOrNull) {
        if (canUndefOrNull === true && (data === null || data === undefined)) {
            return;
        }
        else if (typeof data === 'undefined' || data < min || data > max) {
            return 'Zakres z przedziału ( ' + min + ' : ' + max + ' )';
        }
    };
    $scope.checkUndefOrNull = function (data) {
        if (data === null || data === undefined) {
            return "Wybierz wartość";
        }
    };
    $scope.checkZipCode = function (data) {
        if (data === null || data === undefined || !data.match(/^\d{2}-\d{3}$/)) {
            return "Niewłaściwy format kodu pocztowego";
        }
    };

    $scope.checkListRange = function (data, min, max, canUndefOrNull) {
        //^(\d{1,2}(?:,\d{1,2})*)$
        if (canUndefOrNull === true && (data === null || data === undefined)) {
            return;
        }
        var result = true;
        var list = data.split(',');
        var patt = new RegExp(/^\d{1,2}$/);
        angular.forEach(list, function (value, key) {
            if(!patt.test(value)) {
                result = false;
            } else {
                var r = parseInt(value);
                if (r < min || r > max) {
                    result = false;
                }
            }
        });
        if (result) {
            return;
        }

        return 'Zakres z przedziału ( ' + min + ' : ' + max + ' ) \n w formacie x(,y)*';
    };

    $scope.checkInArray = function(inArray,intVal){
        return inArray.indexOf(intVal)!=-1;
    };
});
