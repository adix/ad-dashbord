/**
 * Created by AndrzejZ on 05.09.2016.
 */
'use strict';
angular.module('myApp.config', ['ngResource']).
config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.headers.common['x-api-key'] = '2fafa7c7-bc4b-411d-8313-0c3468e9ada9';

}]).
constant('PROTOCOL', 'https').
constant('HOST', '192.168.0.20').
constant('PORT', '443').
constant('WS_PORT', '443').
constant('WS_PROT', 'wss').
 constant('SEC_SHOW',true);

//constant('PROTOCOL', 'http').
//constant('HOST', 'localhost').
//constant('PORT', '5000').
//constant('WS_PORT', '8888').
//constant('WS_PROT', 'ws').
//constant('SEC_SHOW',false);
