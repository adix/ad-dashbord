/**
 * Created by anzietek on 2016-01-03.
 */
'use strict';
angular.module('myApp.ReportConfController', ['ngRoute', 'ngResource','myApp.config']).
factory('modifyReportUrl',function(PROTOCOL,HOST,PORT){return PROTOCOL + '://'+HOST+':'+PORT+'/orangepi/api/1.0/report/modify';}).
factory('showReportUrl',function(PROTOCOL,HOST,PORT){return PROTOCOL + '://'+HOST+':'+PORT+'/orangepi/api/1.0/report/show/:id';}).
config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/reportconf', {
        controller: 'ReportConfController',
        templateUrl: 'views/reportconf.html'
    });


}]).factory('ModifyReport', function ($resource, modifyReportUrl) {
    var data = $resource(modifyReportUrl, {}, {
            save: {
                method: 'POST',
                data: '@data'

            },

        },
        {stripTrailingSlashes: true});
    return data;
}).factory('ShowReport', function ($resource, showReportUrl) {
    var data = $resource(showReportUrl, {id: '@id'});
    return data;
}).
controller('ReportConfController', function ($scope, ModifyReport, ShowReport) {
    $scope.selectionTypes = [{id: 0, value: 'Wyzwalanie'},
  //      {id: 1, value: 'Konsola'},
        {id: 2, value: 'Baza danych'},
        {id: 3, value: 'Online'}
    ];
    $scope.type = {};
    $scope.trigger = {};
    $scope.OnOff = [
        {id: false, value: 'Off'},
        {id: true, value: 'On'}
    ];
    $scope.enables = [];


    $scope.enableSend = function (id, data) {
        $scope.enables[id] = true;
    };
    $scope.$watch('type.id', function (data) {
        if (data === 0) {
            ShowReport.get({id: 'trigger'}).$promise.then(function (data) {
                $scope.trigger = data;
            });
        }
        if (data === 1) {
            ShowReport.get({id: 'console'}).$promise.then(function (data) {
                $scope.console = data;
            });
        }
        if (data === 2) {
            ShowReport.get({id: 'mongo'}).$promise.then(function (data) {
                $scope.mongo = data;
                //console.log(data);
            });
        }
        if (data === 3) {
            ShowReport.get({id: 'online'}).$promise.then(function (data) {
                $scope.online = data;

            });
        }
    });
    $scope.buttons = [true,true,true];
    $scope.beforeSave = function (id, form) {
        $scope.buttons[id] = true;
    };
    $scope.edit = function (id, form) {
        $scope.buttons[id] = false;
        form.$show();
    };
    $scope.cancel = function (id, form) {
        $scope.buttons[id] = true;
        form.$cancel();
    };

    $scope.sendToServer = function (id, data) {
        var obj;
        if (id === 0) {

            obj = {report_trigger: data};
        }
        if (id === 1) {

            obj = {console: data};
        }
        if (id === 2) {
            obj = {mongo: data};
        }
        if (id === 3) {
            obj = {online: data};
        }
        // console.log(obj);
        ModifyReport.save(obj);
        $scope.enables[id] = false;
    };
});