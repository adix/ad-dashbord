/**
 * Created by anzietek on 2015-12-17.
 */
'use strict';
angular.module('myApp.OperationController', ['ngRoute', 'ngResource','myApp.config']).
factory('startUrl',function(PROTOCOL,HOST,PORT){return PROTOCOL + '://'+HOST+':'+PORT+'/orangepi/api/1.0/scheduler/start';}).
factory('pauseUrl',function(PROTOCOL,HOST,PORT){return PROTOCOL + '://'+HOST+':'+PORT+'/orangepi/api/1.0/scheduler/pause';}).
factory('resumeUrl',function(PROTOCOL,HOST,PORT){return PROTOCOL + '://'+HOST+':'+PORT+'/orangepi/api/1.0/scheduler/resume';}).
factory('shutdownUrl',function(PROTOCOL,HOST,PORT){return PROTOCOL + '://'+HOST+':'+PORT+'/orangepi/api/1.0/scheduler/shutdown';}).
factory('tasksUrl',function(PROTOCOL,HOST,PORT){return PROTOCOL + '://'+HOST+':'+PORT+'/orangepi/api/1.0/tasks';}).
config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/operation', {
        templateUrl: 'views/operation.html',
        controller: 'OperationController'
    });
}]).
controller('OperationController', function ($scope, $resource, startUrl, pauseUrl, resumeUrl, shutdownUrl, tasksUrl) {

    $scope.startOp = function () {
        $scope.start_task_id = $resource(startUrl).get();
        //console.log($scope.start_task_id);
    };
    $scope.pauseOp = function () {
        $scope.pause_task_id = $resource(pauseUrl).get();
    };
    $scope.resumeOp = function () {
        $scope.resume_task_id = $resource(resumeUrl).get();
    };
    $scope.shutdownOp = function () {
        $scope.shutdown_task_id = $resource(shutdownUrl).get();
    };

    $scope.showJobStatus = function (curr_stat) {
        var promise = $resource(tasksUrl).get().$promise;
        $scope.curr_stat = curr_stat;
        promise.then(function (response) {
            var props = [];
            angular.forEach(response.jobs, function (value, key) {
                if (curr_stat.job_id == value.id) {
                    var properties = Object.keys(value);
                    for (var i = 0, n = properties.length; i < n; i++) {
                        props.push({name: properties[i], value: value[properties[i]]});
                    }
                    $scope.job = props;
                }
            });
        });
    };
    $scope.hideJobStatus = function () {
        $scope.curr_stat.job_id = null;

    };

});